<?php
declare(strict_types=1);
namespace Hola\Entities\Moods;

final class Mood
{
    private $label;
    private $message;

    public function __construct(string $label, string $message)
    {

        $this->label = $label;
        $this->message = $message;
    }
    public function getLabel(): string
    {
        return $this->label;
    }
    public function getMessage(): string
    {
        return $this->message;
    }
}
