<?php
declare(strict_types=1);
namespace Hola\Repositories\Messages;
use Hola\Consts\MessageKeys;
use OutOfBoundsException;

require_once "MessagesInterface.php";
require_once "Consts\MessageKeys.php";

final class Messages implements MessagesInterface
{
    const ERROR = "An error has occured.";
    private $message = [
        MessageKeys::WELCOME => "Hello. What is your mood today?",
        MessageKeys::USER_MOOD_PROMPT => "Please, enter your current mood's number: ",
        MessageKeys::USER_NAME => "Please, enter your first name: ",
    ];

    /** @throws OutOfBoundsException */
    public function getMessage(string $key): string
    {
        if (!array_key_exists($key, $this->message)) {
            throw new OutOfBoundsException($key ." does not exist.");
        }
        return $this->message[$key];
    }
}
