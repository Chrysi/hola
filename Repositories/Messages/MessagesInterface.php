<?php
declare(strict_types=1);
namespace Hola\Repositories\Messages;

interface MessagesInterface
{
    public function getMessage(string $key): string;
}
