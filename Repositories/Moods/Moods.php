<?php
declare(strict_types=1);
namespace Hola\Repositories\Moods;
require_once "MoodsInterface.php";
require_once "Entities/Moods/Mood.php";

use Hola\Entities\Moods\Mood;

final class Moods implements MoodsInterface
{
    private $moods = [];

    public function __construct()
    {
        $this->moods[] = new Mood("great", "I am happy for your mood.");
        $this->moods[] = new Mood("happy", "I am happy with you, too!");
        $this->moods[] = new Mood("ok", "I hope your day will boost your mood.");
        $this->moods[] = new Mood("sad", "I am so sorry... Do you need a virtual hug?");
    }

    public function getList(): array
    {
        return $this->moods;
    }
}
