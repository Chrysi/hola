<?php
declare(strict_types=1);
namespace Hola\Repositories\Moods;

use Hola\Entities\Moods\Mood;

interface MoodsInterface
{
    /** @return Mood[] */
    public function getList(): array;
}
