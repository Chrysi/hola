<?php
declare(strict_types=1);
namespace Hola\Decorators\Moods;

require_once "GreetingMessageInterface.php";

final class GreetingMessage implements GreetingMessageInterface
{

    public function byNameAndMoodMessage(string $username, string $moodMessage): string
    {
        return "Hello " .$username ."! " .$moodMessage;
    }
}