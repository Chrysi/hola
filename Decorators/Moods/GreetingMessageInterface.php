<?php
declare(strict_types=1);
namespace Hola\Decorators\Moods;

interface GreetingMessageInterface
{
    public function byNameAndMoodMessage(string $username, string $moodMessage): string;
}