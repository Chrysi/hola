<?php
declare(strict_types=1);

use Hola\Consts\MessageKeys;
use Hola\Decorators\Moods\GreetingMessage;
use Hola\Repositories\Messages\Messages;
use Hola\Repositories\Moods\Moods;
use Hola\Services\InputReaders\Console;
use Hola\Services\MessagePrinter\StandardMessagePrinter;
use Hola\Services\Moods\Message;
use Hola\Services\Moods\MoodIndex;
use Hola\Services\Moods\Printer;
use Hola\Services\Moods\Reader;
use Hola\Services\Moods\Validator;
use Hola\Services\UserName\Reader as UserNameReader;
use Hola\Services\UserName\Validator as UserNameValidator;

require_once "Repositories\Moods\Moods.php";
require_once "Services\MessagePrinter\StandardMessagePrinter.php";
require_once "Services\Moods\Printer.php";
require_once "Repositories\Messages\Messages.php";
require_once "Consts\MessageKeys.php";
require_once "Services\InputReaders\Console.php";
require_once "Services\Moods\Validator.php";
require_once "Services\Moods\Reader.php";
require_once "Services\UserName\Reader.php";
require_once "Services\UserName\Validator.php";
require_once "Services\Moods\Message.php";
require_once "Services\Moods\MoodIndex.php";
require_once "Decorators\Moods\GreetingMessage.php";

$messagesRepository = new Messages;
try {
    $message = $messagesRepository->getMessage(MessageKeys::WELCOME);
} catch (OutOfBoundsException $exception) {
    die(Messages::ERROR);
}

$messagePrinter = new StandardMessagePrinter();
$messagePrinter->printMessage($message);

$moods = new Moods();
$moodsPrinter = new Printer($moods);
$moodsPrinter->printList();

$readMood = new Reader(new Console(), new Validator($moods), new Messages());
try {
    $userMood = $readMood->getUserMood();
} catch (OutOfBoundsException $exception) {
    die(Messages::ERROR);
}

$readName = new UserNameReader(new Console(), new Messages(), new UserNameValidator());
try {
    $userName = $readName->getUserName();
} catch (OutOfBoundsException $exception) {
    die(Messages::ERROR);
}

$message = new Message($moods, new MoodIndex());
try {
    $moodMessage = $message->getMoodMessage($userMood);
} catch (OutOfBoundsException $exception) {
    die(Messages::ERROR);
}

$greetingMessage = new GreetingMessage();
$finalMessage = $greetingMessage->byNameAndMoodMessage($userName, $moodMessage);
$messagePrinter->printMessage($finalMessage);
