<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

require_once "MoodIndexInterface.php";

final class MoodIndex implements MoodIndexInterface
{

    public function getIndex(int $userMood): int
    {
        return $userMood - 1;
    }
}
