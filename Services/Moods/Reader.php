<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

use Hola\Consts\MessageKeys;
use Hola\Repositories\Messages\MessagesInterface;
use Hola\Services\InputReaders\InputReaderInterface;
use OutOfBoundsException;

require_once "ReaderInterface.php";
require_once "Services\InputReaders\InputReaderInterface.php";
require_once "ValidatorInterface.php";
require_once "Repositories\Messages\Messages.php";
require_once "Consts\MessageKeys.php";

final class Reader implements ReaderInterface
{

    private $inputReader;
    private $validator;
    private $messagesRepository;

    public function __construct(
        InputReaderInterface $inputReader,
        ValidatorInterface $validator,
        MessagesInterface $messagesRepository
    ) {
        $this->inputReader = $inputReader;
        $this->validator = $validator;
        $this->messagesRepository = $messagesRepository;
    }

    /** @throws OutOfBoundsException */
    public function getUserMood(): int
    {
        $userMood = "";
        $promptMessage = $this->messagesRepository->getMessage(MessageKeys::USER_MOOD_PROMPT);
        do {
            $userMood = $this->inputReader->getInput($promptMessage);
            $isChoiceValid = $this->validator->isMoodValid($userMood);
        } while (!$isChoiceValid);

        return intval($userMood);

    }
}
