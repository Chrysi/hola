<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

interface ValidatorInterface
{
    public function isMoodValid(string $mood): bool;
}
