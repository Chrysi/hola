<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

interface MoodIndexInterface
{
    public function getIndex(int $userMood): int;
}
