<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

use Hola\Repositories\Moods\MoodsInterface;
use OutOfBoundsException;

require_once "MessageInterface.php";
require_once "MoodIndexInterface.php";

final class Message implements MessageInterface
{
    private $moodsRepository;
    private $moodIndex;

    public function __construct(
        MoodsInterface $moodsRepository,
        MoodIndexInterface $moodIndex
    ) {
        $this->moodsRepository = $moodsRepository;
        $this->moodIndex = $moodIndex;
    }

    /** @throws OutOfBoundsException */
    public function getMoodMessage(int $userMood): string
    {
        $moodsList = $this->moodsRepository->getList();
        return $moodsList[$this->moodIndex->getIndex($userMood)]->getMessage();

    }
}
