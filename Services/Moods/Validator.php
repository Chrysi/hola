<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

use Hola\Repositories\Moods\MoodsInterface;

require_once "ValidatorInterface.php";
require_once "Repositories/Moods/MoodsInterface.php";


final class Validator implements ValidatorInterface
{
    private $moods;

    public function __construct(MoodsInterface $feeling)
    {

        $this->moods = $feeling;
    }

    public function isMoodValid(string $mood): bool
    {
        if (!is_numeric($mood)) {
            return false;
        }
        $moodsList = $this->moods->getList();
        $listSize = sizeof($moodsList);
        $moodNumber = intval($mood);
        if ($moodNumber < 1 || $moodNumber > $listSize) {
            return false;
        }
        return true;
    }
}
