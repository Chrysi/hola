<?php
declare(strict_types=1);
namespace Hola\Services\Moods;


interface ReaderInterface
{
    public function getUserMood(): int;
}
