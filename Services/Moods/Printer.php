<?php
declare(strict_types=1);
namespace Hola\Services\Moods;
use Hola\Repositories\Moods\MoodsInterface;

require_once "PrinterInterface.php";
require_once "Repositories/Moods/MoodsInterface.php";
require_once "Entities/Moods/Mood.php";


final class Printer implements PrinterInterface
{
    private $moods;

    public function __construct(MoodsInterface $feeling)
    {

        $this->moods = $feeling;
    }

    public function printList()
    {
        $moodsList = $this->moods->getList();
        $i = 1;
        foreach ($moodsList as $mood) {
            echo $i .". " .$mood->getLabel() .PHP_EOL;
            $i++;
        }
    }
}
