<?php
declare(strict_types=1);
namespace Hola\Services\Moods;

interface MessageInterface
{
    public function getMoodMessage(int $userMood): string;
}
