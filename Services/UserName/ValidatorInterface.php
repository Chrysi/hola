<?php
declare(strict_types=1);
namespace Hola\Services\UserName;

interface ValidatorInterface
{
    public function isNameValid(string $userName): bool;
}
