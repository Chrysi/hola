<?php
declare(strict_types=1);
namespace Hola\Services\UserName;


interface ReaderInterface
{
    public function getUserName(): string;
}
