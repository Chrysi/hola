<?php
declare(strict_types=1);
namespace Hola\Services\UserName;

use Hola\Repositories\Messages\MessagesInterface;
use Hola\Consts\MessageKeys;
use Hola\Services\InputReaders\InputReaderInterface;
use OutOfBoundsException;

require_once "ReaderInterface.php";
require_once "Repositories/Messages/MessagesInterface.php";

final class Reader implements ReaderInterface
{

    private $inputReader;
    private $messageRepository;
    private $validator;

    public function __construct(
        InputReaderInterface $inputReader,
        MessagesInterface $messageRepository,
        ValidatorInterface $validator
    ) {

        $this->inputReader = $inputReader;
        $this->messageRepository = $messageRepository;
        $this->validator = $validator;
    }

    /** @throws OutOfBoundsException */
    public function getUserName(): string
    {
        $userInput = "";
        $message = $this->messageRepository->getMessage(MessageKeys::USER_NAME);
        do {
            $userInput = $this->inputReader->getInput($message);
            $isNameValid = $this->validator->isNameValid($userInput);
        } while (!$isNameValid);
        return $userInput;
     }
}
