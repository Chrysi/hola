<?php
declare(strict_types=1);
namespace Hola\Services\UserName;

require_once "ValidatorInterface.php";

final class Validator implements ValidatorInterface
{

    public function isNameValid(string $userName): bool
    {
        if (empty($userName)) {
            return false;
        }

        return ctype_alpha($userName);
    }
}
