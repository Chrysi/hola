<?php
declare(strict_types=1);
namespace Hola\Services\MessagePrinter;

interface MessagePrinterInterface
{
    public function printMessage(string $message);
}
