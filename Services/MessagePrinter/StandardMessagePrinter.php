<?php
declare(strict_types=1);
namespace Hola\Services\MessagePrinter;
require_once "MessagePrinterInterface.php";

final class StandardMessagePrinter implements MessagePrinterInterface
{

    public function printMessage(string $message)
    {
        echo $message .PHP_EOL;
    }
}