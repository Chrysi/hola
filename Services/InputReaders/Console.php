<?php
declare(strict_types=1);
namespace Hola\Services\InputReaders;

require_once "InputReaderInterface.php";

final class Console implements InputReaderInterface
{

    public function getInput(string $prompt): string
    {
        return readline($prompt);
    }
}
