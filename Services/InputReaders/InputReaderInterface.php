<?php
declare(strict_types=1);
namespace Hola\Services\InputReaders;

interface InputReaderInterface
{
    public function getInput(string $prompt): string;
}
