<?php
declare(strict_types=1);
namespace Hola\Consts;

final class MessageKeys
{
    const WELCOME = "Welcome";
    const USER_MOOD_PROMPT = "User mood prompt";
    const USER_NAME = "User name";
}
