<?php
declare(strict_types=1);
require_once "MessagePrinterInterface.php";

final class HappyMessagePrinter implements MessagePrinterInterface
{

    public function printMessage(string $message)
    {
        echo $message ."!" .PHP_EOL;
    }
}
